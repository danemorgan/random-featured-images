<?php
/**
 * Plugin Name: Random Featured Images
 * Plugin URI: http://satishgandham.com/random-featured-images/
 * Description: A simple plugin to set random featured images for posts from media library.
 * Version: 0.1
 * Author: Satish Gandham
 * Author URI: http://SatishGandham.com
 * License: A "Slug" license name e.g. GPL2
 */
 
 /*  Copyright YEAR  PLUGIN_AUTHOR_NAME  (email : PLUGIN AUTHOR EMAIL)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action('admin_menu', 'rfi_admin_page');
function rfi_admin_page() {
add_options_page('Random Thumbnails', 'Random Thumbnails', 'manage_options', 'random-thumbnails', 'rfi_options_page');
}

// display the admin options page
function rfi_options_page() {
?>
<div>
<h2>Random Thumbnails</h2>
A simple plugin to set random featured images for posts from media library.
<h2>Usage</h2>
<ol>
<li>Upload images to your media library via <strong>posts->media</strong>. If you already have enough images in your media library, you can skip this step</li>
<li>Then click the button below to assign those images to your posts</li>
</ol>
<span style="color:red">This plugin is only intended for theme and plugin developers. <br>
It's not possible to undo the changes done by this plugin</span>
<form action="options.php" method="post">
<?php settings_fields('rfi_options'); ?>
<?php do_settings_sections('rfi_plugin'); ?>
 
<input name="Submit" type="submit" value="<?php esc_attr_e('Set Random Thumbnails'); ?>" />
</form></div>
 
<?php
}

add_action('admin_init', 'rfi_admin_init');
function rfi_admin_init(){
register_setting( 'rfi_options', 'rfi_options', 'rfi_options_validate' );
add_settings_section('rfi_options', '', 'rfi_options_section_text', 'rfi_plugin');
add_settings_field('rfi_options', 'Force Assign', 'rfi_options_html', 'rfi_plugin', 'rfi_options');
}

function rfi_options_section_text() {
echo '';
}


function rfi_options_html(){
echo "<br><input id='force' name='rfi_options[force-assign]' size='40' type='checkbox' value='True' /><label for='force'>Check this box to reset featured images for posts that already have them.</label>";

}

function rfi_options_validate($input){
    rfi_set_random_thumbanils((bool)$input['force-assign']);
    return False;
}

function rfi_set_random_thumbanils($force){
    $img_args = array(
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
        'orderby' => 'rand'
    );
    $post_args = array('posts_per_page'=>-1);
    
    $random_images = new WP_Query($img_args);
    $posts = new WP_Query($post_args);
    $count = 0;
    if ($posts->have_posts()) :while ($posts->have_posts()) : $posts->the_post();
        $img = $random_images->next_post();
        if($force || !has_post_thumbnail(get_the_ID()))
        set_post_thumbnail(get_the_ID(), $img->ID);
        $count++;
    endwhile;
    endif;
}
?>